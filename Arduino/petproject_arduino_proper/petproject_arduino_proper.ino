#include "SPI.h"
#include "PN532_SPI.h"
#include "snep.h"
#include "NdefMessage.h"
#include "Wire.h"
#include <Servo.h> 

PN532_SPI pn532spi(SPI, 10);
SNEP nfc(pn532spi);
uint8_t ndefBuf[128];
Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 
int pos = 0;  
boolean runServo = false;
boolean getBitcoin = false;

void setup() {
    Serial.begin(115200);
    myservo.attach(9);
}

void loop() {  
    while(getBitcoin){
      int msgSize = nfc.read(ndefBuf, sizeof(ndefBuf));
      if (msgSize > 0) {
          NdefMessage msg  = NdefMessage(ndefBuf, msgSize);
          //msg.print();
              
          NdefRecord record = msg.getRecord(0);
  
          int payloadLength = record.getPayloadLength();
          byte payload[payloadLength];
          record.getPayload(payload);        
          
          int startChar = 0;        
          if (record.getTnf() == TNF_WELL_KNOWN && record.getType() == "T") { // text message
            // skip the language code
            startChar = payload[0] + 1;
          } else if (record.getTnf() == TNF_WELL_KNOWN && record.getType() == "U") { // URI
            startChar = 1;
          }
                           
          String payloadAsString = "";
          for (int c = startChar; c < payloadLength; c++) {
            payloadAsString += (char)payload[c];
          }
          String actualBitcoin = getValue(payloadAsString, ':', 1);
          Serial.println(actualBitcoin);
          getBitcoin = false;
      } else {
          Serial.println("failed");
      }
      delay(3000);
    }
    if(runServo){
       servoMotor();
       runServo = false;
    }
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read(); 

    if (inChar == '1') {
       getBitcoin = true;
    } else if(inChar == '2'){
       runServo = true;
    }
  }
}

void servoMotor(){
  for(pos = 0; pos < 180; pos += 1)  // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(5);                       // waits 15ms for the servo to reach the position 
  } 
  for(pos = 180; pos>=1; pos-=1)     // goes from 180 degrees to 0 degrees 
  {                                
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(5);                       // waits 15ms for the servo to reach the position 
  } 
}

String getValue(String data, char separator, int index)
{
 int found = 0;
  int strIndex[] = {
0, -1  };
  int maxIndex = data.length()-1;
  for(int i=0; i<=maxIndex && found<=index; i++){
  if(data.charAt(i)==separator || i==maxIndex){
  found++;
  strIndex[0] = strIndex[1]+1;
  strIndex[1] = (i == maxIndex) ? i+1 : i;
  }
 }
  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}
