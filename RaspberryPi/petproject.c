#include <stdio.h>
#include <unistd.h>                     //Used for UART
#include <fcntl.h>                      //Used for UART
#include <termios.h>            //Used for UART
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <lcd.h>
#include <pthread.h>

#define DETECTBTN 29
#define CONFIRMBTN 24

void *threadFunc();
void removeSpaces(char *str1);
void remove_new_line(char* string);

static int lcdHandle;
int rows = 2, cols = 16;

unsigned char tx_buffer[20];
unsigned char *p_tx_buffer;

int readSerial = 0;
int readDetectButton = 0;
int readConfirmButton = 0;
int readZbar = 0;
int s;
char rx_buffer2[256];
char result[256];

int main(int argc, char *argv[]){
	/**********Initialize wiringPi*************************/
 wiringPiSetup();
  	lcdHandle = lcdInit(2, 16, 4,  11, 10, 6,5,4,1,0,0,0,0); //For the new soldered setup!
    if (lcdHandle < 0)
    {
      fprintf (stderr, "%s: lcdInit failed\n", argv [0]);
      return -1;
  }
	    //-------------------------
        //----- SETUP USART 0 -----
        //-------------------------
  int uart0_filestream = -1;

        uart0_filestream = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY | O_NDELAY);          //Open in non blocking read/write mode
        if (uart0_filestream == -1)
        {
            printf("Error - Unable to open UART.  Ensure it is not in use by another application\n");
        }
        struct termios options;
        tcgetattr(uart0_filestream, &options);
        options.c_cflag = B115200 | CS8 | CLOCAL | CREAD;               //<Set baud rate
        options.c_iflag = IGNPAR;
        options.c_oflag = 0;
        options.c_lflag = 0;
        tcflush(uart0_filestream, TCIFLUSH);
        tcsetattr(uart0_filestream, TCSANOW, &options);
    	//Init pins:
        pinMode         (DETECTBTN, INPUT) ;
        pullUpDnControl (DETECTBTN, PUD_UP) ;
        pinMode         (CONFIRMBTN, INPUT) ;
        pullUpDnControl (CONFIRMBTN, PUD_UP) ;




        //----------------------Actual code------------------------//
        while(1){
          lcdClear(lcdHandle);
          lcdPuts(lcdHandle, "Voer fles in");
		//scanButton(DETECTBTN);
          readDetectButton = 1;
          while(readDetectButton > 0){
             if (digitalRead (DETECTBTN) == HIGH) {
				//printf("Detectbutton = high\n");
				delay(10); // Low is pushed
			}
 		 	if (digitalRead (DETECTBTN) == LOW){   // Wait for release
    				//printf("Detectbutton = low\n");
                delay(10);
                readDetectButton = 0;
            }
        }
        lcdClear(lcdHandle);
        lcdPuts(lcdHandle, "Fles gedetecteerd");
        delay(3000);
        lcdClear(lcdHandle);
        lcdPuts(lcdHandle,"Druk op knop om door te gaan?");
        readConfirmButton = 1;
        while(readConfirmButton){
         if (digitalRead (CONFIRMBTN) == HIGH) {
                                //printf("Confirmbutton = high\n");
                                delay(10); // Low is pushed
                            }
                            if (digitalRead (CONFIRMBTN) == LOW){
                                //printf("ConfirmButton = low\n");
                                delay(10);
                                readConfirmButton = 0;
                            }

                        }
                        p_tx_buffer = &tx_buffer[0];
                        *p_tx_buffer++ = '1';
		//Send to arduino that is should start reading nfc chip
                        if (uart0_filestream != -1)
                        {
                        int count = write(uart0_filestream, &tx_buffer[0], (p_tx_buffer - &tx_buffer[0]));              //Filestream, bytes to write, number of bytes to write
                        if (count < 0)
                        {
                            printf("UART TX error\n");
                        }
                        readSerial = 1;
                    }
                    lcdClear(lcdHandle);
                    lcdPuts(lcdHandle,"Scan QR code Of tik NFC");

        //Here below the code to start the thread. Its not really needed to kill the thread..
		//pthread_t pth;	// this is our thread identifier
		//readZbar = 1;
		//s = pthread_create(&pth,NULL,threadFunc,"processing...");

                    while(readSerial > 0){
                        //----- CHECK FOR ANY RX BYTES -----

                        if (uart0_filestream != -1)
                        {
                                // Read up to 255 characters from the port if they are there
                            char rx_buffer[256];

                                int rx_length = read(uart0_filestream, (void*)rx_buffer, 255);          //Filestream, buffer to store in, number of bytes to read (max)
                                if (rx_length < 0)
                                {
                                //An error occured (will occur if there are no bytes)
                                }
                                else if (rx_length == 0)
                                {
                                        //No data waiting
                                }
                                else
                                {

                                        //Bytes received
                                    rx_buffer[rx_length] = '\0';
                                    printf("%s\n", rx_buffer);
                    					//int rxLength = sizeof(rx_buffer);
                    					//printf("Length of buffer: %i\n", strlen(rx_buffer));
                    					//if buffer is a bitcoin then readSerial = 0; and start servo (go out of the while loop)
                                    if(strlen(rx_buffer) > 10){
                                      lcdClear(lcdHandle);
                                      lcdPuts(lcdHandle, "NFC Scanned");
                    						//lcdPuts(lcdHandle, rx_buffer);
                                      readZbar = 0;
                                      size_t destination_size = sizeof (rx_buffer2);

                                      strncpy(rx_buffer2, rx_buffer, destination_size);
                    						//rx_buffer2[destination_size - 1] = '\0';
                    						//rx_buffer2 = rx_buffer;
                                      remove_new_line(rx_buffer2);
                                      removeSpaces(rx_buffer2);
                                      readSerial = 0;
                                  }
                              }
                          }
                      }
                //Below is the bitcoin code. This already works. But since i wanted to test the POC when it was offline i commented this code
                //
        //		char str[100];
        //		strcpy(str, "bitcoin-cli sendtoaddress ");
        //		strcat(str, rx_buffer2);
        //		strcat(str, " 0.1");
        //		printf("%s\n", str);
        //		FILE *tp;
          //  tp = popen(str, "r");
        //
          //  if(!tp) {
        //        fprintf(stderr, "Error opening pipe.\n");
          //      //return 1;
        //    }
        //
        //    while(!feof(tp)) {
          //      //printf("%c", fgetc(tp));
        //    }

        //    if (pclose(tp) == -1) {
         //       fprintf(stderr," Error!\n");
          //      //return 1;
           // }

		//send start signal for servo on arduihno
                      p_tx_buffer = &tx_buffer[0];
                      *p_tx_buffer++ = '2';
                      if (uart0_filestream != -1)
                      {
                int count = write(uart0_filestream, &tx_buffer[0], (p_tx_buffer - &tx_buffer[0]));              //Filestream, bytes to write, number of bytes to write
                if (count < 0)
                {
                    printf("UART TX error\n");
                }
            }
		//delay(10000);
            printf("Simulating things");
            delay(2000);
            printf("after the delay..");
            lcdClear(lcdHandle);
            lcdPuts(lcdHandle, "Simulating Chop bottle cap");
            delay(10000);
		//pthread_join(pth,NULL);
            printf("start over again!");
        }
        close(uart0_filestream);
    }

    void *threadFunc()
    {   
        //Keep this thread alive forever.. and just read the value
        while(1){
            //You should set the readZbar value to 1 if you want to read the webcam
           while(readZbar > 0){

               FILE *fp;
               fp = popen("sudo zbarcam --raw --prescale=300x300 /dev/video0 --nodisplay", "r");

               int i;
               char c;


               if(!fp) {
                fprintf(stderr, "Error opening file.\n");
                return 1;
                }

                i = 0;
                while((c = fgetc(fp)) != EOF) {
                    result[i] = c;
                    i++;
                }
                result[i] = '\0';
                //Here the result gets printed. You can just get the 'result' value and put it in the bitcoin thing.
                //printf("%s", result);
               // i = atoi(result);
                //printf("%d", i);

                if (fclose(fp) == -1) {
                    fprintf(stderr," Error closing file!\n");
                    return 1;
                }
            }
        }

    printf("end of thread and it will return 0");
    return 0;
}

void removeSpaces(char *str1)
{
    char *str2;
    str2=str1;
    while (*str2==' ') str2++;
    if (str2!=str1) memmove(str1,str2,strlen(str2)+1);
}

void remove_new_line(char* string){
  int length = strlen(string);
  while(length>0){
    if(string[length-1] == '\n'){
        --length;
        string[length] ='\0';
    }else
    break;
}
}

