#!/bin/bash
/usr/bin/sudo apt-get update
/usr/bin/sudo apt-get upgrade
/usr/bin/sudo apt-get install build-essential autoconf libssl-dev libboost-dev libboost-chrono-dev libboost-filesystem-dev libboost-program-options-dev libboost-system-dev libboost-test-dev libboost-thread-dev
/usr/bin/sudo mkdir /home/pi/bin
/usr/bin/sudo cd /home/pi/bin
/usr/bin/sudo/wget http://download.oracle.com/berkeley-db/db-4.8.30.NC.tar.gz
/usr/bin/sudo tar -xzvf db-4.8.30.NC.tar.gz
/usr/bin/sudo cd db-4.8.30.NC/build_unix/
/usr/bin/sudo ./dist/configure --enable-cxx
/usr/bin/sudo make
/usr/bin/sudo make install
/usr/bin/sudo cd /home/pi/bin
/usr/bin/sudo git clone -b 0.9.3 https://github.com/bitcoin/bitcoin.git
/usr/bin/sudo cd bitcoin/
/usr/bin/sudo ./autogen.sh
/usr/bin/sudo ./configure CPPFLAGS="-I/usr/local/BerkeleyDB.4.8/include -O2" LDFLAGS="-L/usr/local/BerkeleyDB.4.8/lib"
/usr/bin/sudo make
/usr/bin/sudo make install
/usr/bin/sudo
/usr/bin/sudo
/usr/bin/sudo
exit