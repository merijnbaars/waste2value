#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <wiringPi.h>
#include <assert.h>
#include <lcd.h>

#define DETECTBTN 24
static int lcdHandle;
int rows = 2, cols = 16;

int readDetectButton = 0;
int main(int argc, char *argv[])
{
wiringPiSetup();
  	lcdHandle = lcdInit(2, 16, 4,  11, 10, 6,5,4,1,0,0,0,0); //For the new soldered setup!
        if (lcdHandle < 0)
  	{
    		fprintf (stderr, "%s: lcdInit failed\n", argv [0]);
    		return -1;
  	}

pinMode         (DETECTBTN, INPUT) ;
   	pullUpDnControl (DETECTBTN, PUD_UP) ;

    struct ifaddrs *ifaddr, *ifa;
    int family, s;
    char host[NI_MAXHOST];

    if (getifaddrs(&ifaddr) == -1)
    {
        perror("getifaddrs");
        exit(EXIT_FAILURE);
    }


    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
    {
        if (ifa->ifa_addr == NULL)
            continue;

        s=getnameinfo(ifa->ifa_addr,sizeof(struct sockaddr_in),host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);

        if((strcmp(ifa->ifa_name,"eth0")==0)&&(ifa->ifa_addr->sa_family==AF_INET))
        {
            if (s != 0)
            {
                printf("getnameinfo() failed: %s\n", gai_strerror(s));
                exit(EXIT_FAILURE);
            }
	//lcdPuts(lcdHandle, "Interface: ");
	//lcdPosition(lcdHandlle, 11,0);
	lcdPrintf(lcdHandle,ifa->ifa_name);
	lcdPosition(lcdHandle, 0, 1);
	lcdPuts(lcdHandle, host);

            printf("\tInterface : <%s>\n",ifa->ifa_name );
            printf("\t  Address : <%s>\n", host);
        }
    }
	readDetectButton = 1;
		while(readDetectButton > 0){
			if (digitalRead (DETECTBTN) == HIGH) {
				//printf("Detectbutton = high\n");
				delay(10); // Low is pushed
			}
 		 	if (digitalRead (DETECTBTN) == LOW){   // Wait for release
    				//printf("Detectbutton = low\n");
				delay(10);
				readDetectButton = 0;
			}
		}
lcdClear(lcdHandle);
    freeifaddrs(ifaddr);
    exit(EXIT_SUCCESS);
}
